package com.huaqing.util;

import com.huaqing.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

@Slf4j
public class DataOperationImplTest {
    private User user;

    @Before
    public void setUp() throws Exception {
        user = new User();
        user.setName("test name");
    }

    @Test
    public void dataToFileExport() {
        DataOperationImpl<User> dataOperation = new DataOperationImpl();
        int num = dataOperation.dataToFileExport(user);
        assertEquals(1,num);
    }

    @Test
    public void fileToDataImport() {
        DataOperationImpl dataOperation = new DataOperationImpl();
        String filePatch = "./data/"+User.class.getName()+".txt";
        File file = new File(filePatch);
        Object o = dataOperation.fileToDataImport(file);
        User user = (User) o;
        assertEquals(this.user,user);
    }
}