package huaqing.util;

import com.huaqing.util.DataOperationImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.File;

/**
 * @program: javatrainingfirstweek
 * @Description:
 * @author: 24562
 * @date: 2022/4/12 23:22
 */
@Slf4j
public class DataOperationTest {

    @Test
    public void dataToFileExportTest(){
        DataOperationImpl dataOperation = new DataOperationImpl();
        int num = dataOperation.dataToFileExport("dsaas");
        if (num==1){
            log.info("写入成功");
        }else {
            log.info("写入失败");
        }
    }

    @Test
    public void fileToDataImportTest(){
        DataOperationImpl dataOperation = new DataOperationImpl();
        String filePatch = "./data_java.lang.String.txt";
        File file = new File(filePatch);
        Object o = dataOperation.fileToDataImport(file);
        log.info(String.valueOf(o));
    }
}
