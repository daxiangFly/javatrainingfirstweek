import com.huaqing.init.Init;
import com.huaqing.play.GameThread;
import lombok.extern.slf4j.Slf4j;
import com.huaqing.play.Play;
import com.huaqing.pojo.Machine;
import com.huaqing.pojo.User;
import com.huaqing.save.LazyMan;
import com.huaqing.util.TxtUtil;

import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: javatrainingfirstweek
 * @Description:
 * @author: 24562
 * @date: 2022/4/8 17:21
 */
@Slf4j
public class Demo {
    public static void main(String[] args) throws InterruptedException {

        GameThread game = new GameThread();
        //使用线程池
        ExecutorService threadPool = new ThreadPoolExecutor(
                0,
                5,
                10,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue(1),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.DiscardPolicy());

        threadPool.execute(game);
        threadPool.execute(game);
        threadPool.execute(game);

//        Thread playGame = new Thread(game);
//        playGame.start();
        //----------------
//        Init init = new Init();
//        Scanner sc = new Scanner(System.in);
//        Play play = new Play();
//        log.info("请输入玩几局");
//        Integer n = sc.nextInt();
//        user = init.userInit(user, n);
//        machine = init.machineInit(machine, n);
//        log.info("机器出:"+Arrays.toString(machine.getOperations()));
//        String[] res = play.compare(user, machine);
//        log.info("结果为:"+Arrays.toString(res));
//        LazyMan save = LazyMan.getInstance();
//        save.saveResults(res);
//        log.info("平局:"+save.getEven());
//        log.info("胜:"+save.getUserWin());
//        log.info("负:"+save.getUserDefeat());
//        TxtUtil txtUtil = new TxtUtil();
//        txtUtil.writer(save);
//        txtUtil.ReadData(save);
    }
}
