package com.huaqing.util;

import java.io.File;

/**
 * @program: javatrainingfirstweek
 * @Description: 数据操作，如数据的导入和导出
 * @author: Mr.Cheng
 * @date: 2022/4/10 4:30 下午
 */
public interface DataOperation<T> {
    /**
    * @Description: data export to a file
    * @Param: [data]
    * @return: int
    * @Author: Mr.Cheng
    * @Date: 2022/4/10 4:33 下午
    */
    int dataToFileExport(T data);

    /**
    * @Description: data import from a file
    * @Param: [file]
    * @return: T
    * @Author: Mr.Cheng
    * @Date: 2022/4/10 4:34 下午
    */
    T fileToDataImport(File file);
}
