package com.huaqing.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * @program: javatrainingfirstweek
 * @Description:
 * @author: Mr.Cheng
 * @date: 2022/4/12 9:52 下午
 */
@Slf4j
public class DataOperationImpl<T> implements DataOperation<T>{
    /**
     * @param data
     * @Description: data export to a file
     * @Param: [data]
     * @return: int
     * @Author: Mr.Cheng
     * @Date: 2022/4/10 4:33 下午
     */
    @Override
    public int dataToFileExport(T data) {
        StringBuilder filePath = new StringBuilder();
        filePath.append("./data/").append(data.getClass().getName()).append(".txt");
        File file = new File(filePath.toString());
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                log.error("can not create file");
            }
        }
        String json= JSON.toJSONString(data, SerializerFeature.WRITE_MAP_NULL_FEATURES);
        /** 使用try-resource的写法,不把路径写死 **/
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(json);
            return 1;
        } catch (IOException e) {
            log.error("can not write to file",e);
        }
        return 0;
    }

    /**
     * @param file
     * @Description: data import from a file
     * @Param: [file]
     * @return: T
     * @Author: Mr.Cheng
     * @Date: 2022/4/10 4:34 下午
     */
    @Override
    public T fileToDataImport(File file) {
        StringBuffer row = new StringBuffer();
        try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String temp = "";
            while((temp = reader.readLine())!=null){
                row.append(temp);
            }
        } catch (FileNotFoundException e) {
            log.error("can not find file",e);
        } catch (IOException e){
            log.error("can not read file",e);
        }
        T t = JSON.parseObject(row.toString(), new TypeReference<T>() {
        });
        JSONObject jsonObject = JSON.parseObject(row.toString());
//        List<Integer> operations = JSON.parseArray(jsonObject.getString("operations"), Integer.class);
        return (T) jsonObject;
    }
}
