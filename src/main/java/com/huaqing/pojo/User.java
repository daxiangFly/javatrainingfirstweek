package com.huaqing.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: javatrainingfirstweek
 * @Description:
 * @author: 24562
 * @date: 2022/4/8 16:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String name = "user";
    private Integer[] operations;

}
