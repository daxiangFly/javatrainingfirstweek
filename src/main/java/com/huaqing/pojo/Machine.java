package com.huaqing.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: javatrainingfirstweek
 * @Description:
 * @author: 24562
 * @date: 2022/4/8 16:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Machine {
    private String name = "machine";
    private Integer[] operations;
}
