package com.huaqing.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: javatrainingfirstweek
 * @Description:
 * @author: 24562
 * @date: 2022/4/16 15:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    private String winnerName;
    private Integer win = 0;
    private Integer defeat = 0;
    private Integer even = 0;
}
