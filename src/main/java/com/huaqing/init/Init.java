package com.huaqing.init;

import lombok.extern.slf4j.Slf4j;
import com.huaqing.pojo.Machine;
import com.huaqing.pojo.User;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Scanner;

/**
 * @program: javatrainingfirstweek
 * @Description:
 * @author: 24562
 * @date: 2022/4/8 17:12
 */
@Slf4j
public class Init {
    public User userInit(User user,Integer n){
        log.info("Note: 出1为石头\t出2为剪刀\t出3为布");
        Scanner sc = new Scanner(System.in);
        Integer[] operations = new Integer[n];
        for (int i = 0; i < operations.length; i++) {
            operations[i] = sc.nextInt();
        }
        user.setOperations(operations);
        return user;
    }

    public Machine machineInit(Machine machine,Integer n){
        Integer[] operations = new Integer[n];
        try {
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            for (int i = 0; i < operations.length; i++) {
                operations[i] = random.nextInt(3)+1;
            }
            machine.setOperations(operations);
            machine.setName(machine.getName()+random.nextInt(10));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return machine;
    }
}
