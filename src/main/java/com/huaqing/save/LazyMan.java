package com.huaqing.save;

import lombok.Data;

/**
 * @program: javatrainingfirstweek
 * @Description: 线程不安全，此种方式
 * @author: 24562
 * @date: 2022/4/8 16:46
 */
@Data
public class LazyMan {
    private LazyMan(){}
    private static LazyMan lazyMan;
    private Integer userWin = 0;
    private Integer userDefeat = 0;
    private Integer even = 0;

    public static LazyMan getInstance(){
        if (lazyMan==null){
            lazyMan = new LazyMan();
        }
        return lazyMan;
    }

    public void saveResults(String[] res) {
        for (int i = 0; i < res.length; i++) {
            if (res[i].equals("user")){
                userWin++;
            }else if (res[i].equals("machine")){
                userDefeat++;
            }else {
                even++;
            }
        }
    }
}
