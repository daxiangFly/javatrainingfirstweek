package com.huaqing.play;

import com.huaqing.pojo.Machine;
import com.huaqing.pojo.User;

/**
 * @program: javatrainingfirstweek
 * @Description:
 * @author: 24562
 * @date: 2022/4/8 16:53
 */
public class Play {
    public String[] compare(Machine machine, Machine machine2){
        Integer[] Operations = machine.getOperations();
        Integer[] Operations2 = machine2.getOperations();
        String[] results = new String[Operations.length];
        for (int i = 0; i < Operations.length; i++) {
            int rs = Operations[i] - Operations2[i];
            switch (rs) {
                case -1:
                case 2:
                    results[i] = machine.getName();
                    break;
                case 0:
                    results[i] = "平局";
                    break;
                case 1:
                case -2:
                    results[i] = machine2.getName();
                    break;
                default:
                    break;
            }
        }
        return results;
    }

}
