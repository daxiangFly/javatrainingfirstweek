package com.huaqing.play;

import com.huaqing.init.Init;
import com.huaqing.pojo.Machine;
import com.huaqing.pojo.Result;
import com.huaqing.pojo.User;
import com.huaqing.save.SingletonMemory;
import com.huaqing.util.DataOperationImpl;
import com.huaqing.util.TxtUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: javatrainingfirstweek
 * @Description:
 * @author: Mr.Cheng
 * @date: 2022/4/11 7:22 下午
 */
@Slf4j
public class GameThread implements Runnable{
//    private  User user;
//    private Machine machine;
//    private Machine machine2;
//
//    public GameThread(Machine machine, Machine machine2) {
////        this.user = user;
//        this.machine = machine;
//        this.machine2 = machine2;
//    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        Machine machine = new Machine();
        Machine machine2 = new Machine();

        Init init = new Init();
        Scanner sc = new Scanner(System.in);
        Play play = new Play();
        log.info(Thread.currentThread().getName());
//        log.info("请输入玩几局");
//        Integer n = sc.nextInt();
//            user = init.userInit(user, n);
        Integer n = 5;
        machine = init.machineInit(machine, n);
        machine2 = init.machineInit(machine2, n);
        log.info("先手->"+machine.getName()+":"+ Arrays.toString(machine.getOperations()));
        log.info("后手->"+machine2.getName()+":"+ Arrays.toString(machine2.getOperations()));
        String[] res = play.compare(machine, machine2);
        log.info("结果为:"+Arrays.toString(res));
        SingletonMemory save = SingletonMemory.getSingleton();
        save.saveResults(res);
        List<Result> resultList = save.getResultList();
        log.info("胜利者:"+ resultList.get(0).getWinnerName());
        log.info("平局:"+resultList.get(0).getEven());
        log.info("胜:"+resultList.get(0).getWin());
        log.info("负:"+resultList.get(0).getDefeat());
        DataOperationImpl dataOperation = new DataOperationImpl();
        int i = dataOperation.dataToFileExport(save);
        if (i==1){
            log.info("写入文件成功");
            log.info(dataOperation.fileToDataImport(new File("./data/" + save.getClass().getName() + ".txt")).toString());
        }else {
            log.info("写入文件失败");
        }
    }
}
